Start Day and Time: 2024-05-28 at 18:22:15 (-0800)

2024-05-28 at 18:22:15 (-0800): Init Network Data
2024-05-28 at 18:22:15 (-0800): Init random data generator
2024-05-28 at 18:22:15 (-0800): Init network results
2024-05-28 at 18:22:15 (-0800): Prepare paremeters
2024-05-28 at 18:22:15 (-0800): *****************************************************************************************************
2024-05-28 at 18:22:15 (-0800): Prepare parameters of network project started...
2024-05-28 at 18:22:15 (-0800): *****************************************************************************************************
2024-05-28 at 18:22:15 (-0800): ****************************************************************
2024-05-28 at 18:22:15 (-0800): Preparation of transmitters for prediction started...
2024-05-28 at 18:22:15 (-0800): ****************************************************************
2024-05-28 at 18:22:15 (-0800):     Set pointer from transmitters to sites started...
2024-05-28 at 18:22:15 (-0800):         Antenna "EBC-DD1"
2024-05-28 at 18:22:15 (-0800):     Set pointer from transmitters to sites done
2024-05-28 at 18:22:15 (-0800):     Determine min. antenna height at each site started...
2024-05-28 at 18:22:15 (-0800):         Antenna "EBC-DD1"
2024-05-28 at 18:22:15 (-0800):     Determine min. antenna height at each site done
2024-05-28 at 18:22:15 (-0800):     Determine apertures of antennas started...
2024-05-28 at 18:22:15 (-0800):         Aperture of antennna "EBC-DD1" is 360.00�
2024-05-28 at 18:22:15 (-0800):     Determine apertures of antennnas done
2024-05-28 at 18:22:15 (-0800):     Remove disabled transmitters from list
2024-05-28 at 18:22:15 (-0800):     Copy default settings of transmitters started
2024-05-28 at 18:22:15 (-0800):         Antenna "EBC-DD1"
2024-05-28 at 18:22:15 (-0800):     Copy default settings of transmitters done
2024-05-28 at 18:22:15 (-0800):     Check azimuth of antenna patterns started...
2024-05-28 at 18:22:15 (-0800):         Antenna "EBC-DD1"
2024-05-28 at 18:22:15 (-0800):     Check azimuth of antenna patterns done
2024-05-28 at 18:22:15 (-0800):     Define prediction areas started...
2024-05-28 at 18:22:15 (-0800):         Antenna "EBC-DD1"
2024-05-28 at 18:22:15 (-0800):     Define prediction areas done
2024-05-28 at 18:22:15 (-0800):     Check power settings started...
2024-05-28 at 18:22:15 (-0800):         Antenna "EBC-DD1"
2024-05-28 at 18:22:15 (-0800):     Check power settings done
2024-05-28 at 18:22:15 (-0800):     Tramsmitters counted (1 transmitters)
2024-05-28 at 18:22:15 (-0800): ****************************************************************
2024-05-28 at 18:22:15 (-0800): Preparation of transmitters for prediction completed
2024-05-28 at 18:22:15 (-0800): ****************************************************************
2024-05-28 at 18:22:15 (-0800):     Check if at least one carrier is assigned to each transmitter/cell started...
2024-05-28 at 18:22:15 (-0800):         transmitter/cell "EBC-DD1"
2024-05-28 at 18:22:15 (-0800):     Check if at least one carrier is assigned to each transmitter/cell completed
2024-05-28 at 18:22:15 (-0800): *****************************************************************************************************
2024-05-28 at 18:22:15 (-0800): Prepare parameters of network project completed
2024-05-28 at 18:22:15 (-0800): *****************************************************************************************************
2024-05-28 at 18:22:15 (-0800): Check the results to be computed
2024-05-28 at 18:22:15 (-0800): Init structures for results
2024-05-28 at 18:22:15 (-0800): Start with propagation predictions...
2024-05-28 at 18:22:15 (-0800): **************************************************************************************
2024-05-28 at 18:22:15 (-0800):    Checking settings of transmitter "EBC-DD1"....
2024-05-28 at 18:22:15 (-0800): **************************************************************************************
2024-05-28 at 18:22:15 (-0800): **************************************************************************************
2024-05-28 at 18:22:15 (-0800):    Checking settings of transmitter     "EBC-DD1"   done
2024-05-28 at 18:22:15 (-0800):      => Transmitter is ok
2024-05-28 at 18:22:15 (-0800): **************************************************************************************
2024-05-28 at 18:22:15 (-0800):  **************************************************************************************************************
2024-05-28 at 18:22:15 (-0800):    EBC-DD1 
2024-05-28 at 18:22:15 (-0800):  **************************************************************************************************************
2024-05-28 at 18:22:15 (-0800):  => Start with prediction of transmitter/antenna/cell
2024-05-28 at 18:22:15 (-0800):  **************************************************************************************************************
2024-05-28 at 18:22:15 (-0800): "Y:\work\att_networking_sims\emu_exp_prop\EBC-DD1\Rays.str" deleted because of re-computation
2024-05-28 at 18:22:15 (-0800): "Y:\work\att_networking_sims\emu_exp_prop\EBC-DD1\Rays.ray" deleted because of re-computation
2024-05-28 at 18:22:15 (-0800): "Y:\work\att_networking_sims\emu_exp_prop\EBC-DD1\Power.fpp" deleted because of re-computation
2024-05-28 at 18:22:15 (-0800): "emu_exp_MS\EBC-DD1\Power.fpp" deleted because of re-computation
2024-05-28 at 18:22:15 (-0800): "Y:\work\att_networking_sims\emu_exp_prop\EBC-DD1\Path Loss.fpl" deleted because of re-computation
2024-05-28 at 18:22:15 (-0800): "emu_exp_MS\EBC-DD1\Delay Spread.fpd" deleted because of re-computation
2024-05-28 at 18:22:15 (-0800): "Y:\work\att_networking_sims\emu_exp_prop\EBC-DD1\LOS.los" deleted because of re-computation
2024-05-28 at 18:22:15 (-0800): Start of Urban Computation
2024-05-28 at 18:22:15 (-0800): Init data done
2024-05-28 at 18:22:15 (-0800): Check project parameters started....
2024-05-28 at 18:22:15 (-0800):   Checking Parameters
2024-05-28 at 18:22:15 (-0800):   Checking Lines
2024-05-28 at 18:22:15 (-0800):   Copy data of default TRX and remove default TRX from list
2024-05-28 at 18:22:15 (-0800): ****************************************************************
2024-05-28 at 18:22:15 (-0800): Preparation of transmitters for prediction started...
2024-05-28 at 18:22:15 (-0800): ****************************************************************
2024-05-28 at 18:22:15 (-0800):     Check properties of new transmitter
2024-05-28 at 18:22:15 (-0800):     Check properties of default transmitter
2024-05-28 at 18:22:15 (-0800):     Set pointer from transmitters to sites started...
2024-05-28 at 18:22:15 (-0800):         Antenna "EBC-DD1"
2024-05-28 at 18:22:15 (-0800):     Set pointer from transmitters to sites done
2024-05-28 at 18:22:15 (-0800):     Determine min. antenna height at each site started...
2024-05-28 at 18:22:15 (-0800):         Antenna "EBC-DD1"
2024-05-28 at 18:22:15 (-0800):     Determine min. antenna height at each site done
2024-05-28 at 18:22:15 (-0800):     Determine apertures of antennas started...
2024-05-28 at 18:22:15 (-0800):         Aperture of antennna "EBC-DD1" is 360.00�
2024-05-28 at 18:22:15 (-0800):     Determine apertures of antennnas done
2024-05-28 at 18:22:15 (-0800):     Check azimuth of antenna patterns started...
2024-05-28 at 18:22:15 (-0800):     Check azimuth of antenna patterns done
2024-05-28 at 18:22:15 (-0800):     Define prediction areas started...
2024-05-28 at 18:22:15 (-0800):         Antenna "EBC-DD1"
2024-05-28 at 18:22:15 (-0800):     Define prediction areas done
2024-05-28 at 18:22:15 (-0800):     Check power settings started...
2024-05-28 at 18:22:15 (-0800):         Antenna "EBC-DD1"
2024-05-28 at 18:22:15 (-0800):     Check power settings done
2024-05-28 at 18:22:15 (-0800):     Tramsmitters counted (1 transmitters)
2024-05-28 at 18:22:15 (-0800): ****************************************************************
2024-05-28 at 18:22:15 (-0800): Preparation of transmitters for prediction completed
2024-05-28 at 18:22:15 (-0800): ****************************************************************
2024-05-28 at 18:22:15 (-0800):   Check TRX settings
2024-05-28 at 18:22:15 (-0800):   Check computation mode
2024-05-28 at 18:22:15 (-0800):   Check delay spread settings
2024-05-28 at 18:22:15 (-0800):   Check default material properties
2024-05-28 at 18:22:15 (-0800):   Check prediction area
2024-05-28 at 18:22:15 (-0800):   Check transition between IRT and COST postprocessing
2024-05-28 at 18:22:15 (-0800):   Checks successful => Continue with further computation!
2024-05-28 at 18:22:15 (-0800): Check project parameters done.
2024-05-28 at 18:22:15 (-0800): Copy Project Parameters started....
2024-05-28 at 18:22:15 (-0800):    Copy location of transmitters
2024-05-28 at 18:22:15 (-0800):    Copy further parameters started...
2024-05-28 at 18:22:15 (-0800):        Copy further parameters
2024-05-28 at 18:22:15 (-0800):        Copy settings of area mode
2024-05-28 at 18:22:15 (-0800):        Copy breakpoint settings (2.600, 3.300)
2024-05-28 at 18:22:15 (-0800):        Copy settings of postprocessing
2024-05-28 at 18:22:15 (-0800):        Copy settings of empirical indoor coverage
2024-05-28 at 18:22:15 (-0800):        Copy CNP settings
2024-05-28 at 18:22:15 (-0800):        Copy parameters done
2024-05-28 at 18:22:15 (-0800):    Copy further parameters done.
2024-05-28 at 18:22:15 (-0800):    Copy parameters successfully completed.
2024-05-28 at 18:22:15 (-0800): Copy Project Parameters done.
2024-05-28 at 18:22:15 (-0800): Preparation of antenna patttern of "EBC-DD1" started....
2024-05-28 at 18:22:15 (-0800): Read antenna pattern "Y:\work\feko_files\antennas\VVSSP\msi\VVSSP-360S-F_SBandCombined_00DT_3450"
2024-05-28 at 18:22:15 (-0800): Antenna pattern "Y:\work\feko_files\antennas\VVSSP\msi\VVSSP-360S-F_SBandCombined_00DT_3450.msi"
2024-05-28 at 18:22:15 (-0800): successfully read from disk!
2024-05-28 at 18:22:15 (-0800): Preparation of antenna patttern of "EBC-DD1" completed
2024-05-28 at 18:22:15 (-0800): Calling dedicated urban propagation module...
2024-05-28 at 18:22:15 (-0800):     Urban Propagation Model started...
2024-05-28 at 18:22:15 (-0800):     Init coordinate system
2024-05-28 at 18:22:15 (-0800):     Init prediction area
2024-05-28 at 18:22:15 (-0800):     Init interface
2024-05-28 at 18:22:15 (-0800):     Calling propagation engine ...
2024-05-28 at 18:22:15 (-0800): Starting with computation of transmitter "EBC-DD1"
2024-05-28 at 18:22:16 (-0800): Different coordinate ellipsoid in building and topo database defined!
2024-05-28 at 18:22:16 (-0800):   => Determination of absolute height of buildings (incl. topography) started...
2024-05-28 at 18:22:16 (-0800):   => Determination of absolute height of buildings (incl. topography) completed
2024-05-28 at 18:22:17 (-0800): Start ITU-R P.1411 Prediction..... 
2024-05-28 at 18:22:17 (-0800):     Start the loop around the prediction area till all pixels are reached
2024-05-28 at 18:22:17 (-0800):   0% 
2024-05-28 at 18:22:17 (-0800): Site-Specific over roof-top propagation model (urban areas) is valid
2024-05-28 at 18:22:17 (-0800):  for a distance range between 20 to 5000 m!
2024-05-28 at 18:22:23 (-0800):  10%  ITU-R P.1411 prediction
2024-05-28 at 18:22:33 (-0800):  20%  ITU-R P.1411 prediction
2024-05-28 at 18:22:43 (-0800):  30%  ITU-R P.1411 prediction
2024-05-28 at 18:22:54 (-0800):  40%  ITU-R P.1411 prediction
2024-05-28 at 18:23:09 (-0800):  50%  ITU-R P.1411 prediction
2024-05-28 at 18:23:26 (-0800):  60%  ITU-R P.1411 prediction
2024-05-28 at 18:23:43 (-0800):  70%  ITU-R P.1411 prediction
2024-05-28 at 18:23:58 (-0800):  80%  ITU-R P.1411 prediction
2024-05-28 at 18:24:06 (-0800):  90%  ITU-R P.1411 prediction
2024-05-28 at 18:24:12 (-0800): 100%  ITU-R P.1411 prediction
2024-05-28 at 18:24:12 (-0800):     Loop around the prediction area is completed. All pixels are reached
2024-05-28 at 18:24:12 (-0800): ITU-R P.1411 Prediction successfully completed
2024-05-28 at 18:24:12 (-0800): LOS computation started...
2024-05-28 at 18:24:12 (-0800):     Compute LOS status for each pixel in result
2024-05-28 at 18:24:14 (-0800):     Status of outdoor pixels:
2024-05-28 at 18:24:14 (-0800):         LOS      22157 pixel
2024-05-28 at 18:24:14 (-0800):         NLOS    132639 pixel
2024-05-28 at 18:24:14 (-0800):         VLOS     4866 pixel
2024-05-28 at 18:24:14 (-0800):         VNLOS   116468 pixel
2024-05-28 at 18:24:14 (-0800):         Total   276130 pixel
2024-05-28 at 18:24:14 (-0800): LOS computation completed.
2024-05-28 at 18:24:14 (-0800):     Propagation engine successfully completed
2024-05-28 at 18:24:14 (-0800):     Free memory started...
2024-05-28 at 18:24:14 (-0800): Free memory for locations
2024-05-28 at 18:24:14 (-0800):     Free memory completed.
2024-05-28 at 18:24:14 (-0800): Close Files
2024-05-28 at 18:24:14 (-0800): Urban Module finished successfully
2024-05-28 at 18:24:14 (-0800): Returning from dedicated urban propagation module.
2024-05-28 at 18:24:14 (-0800): Prepare internal postprocessing of data
2024-05-28 at 18:24:14 (-0800): Writing results....
2024-05-28 at 18:24:14 (-0800): Results written.
2024-05-28 at 18:24:14 (-0800): Urban Propagation Module successfully completed
2024-05-28 at 18:24:14 (-0800): Writing file "Y:\work\att_networking_sims\emu_exp_prop\EBC-DD1\Power.txt"....
2024-05-28 at 18:24:14 (-0800): Write file "Y:\work\att_networking_sims\emu_exp_prop\EBC-DD1\Power.txt" successfully completed!
2024-05-28 at 18:24:14 (-0800): Writing file "Y:\work\att_networking_sims\emu_exp_prop\EBC-DD1\Power.fpp"....
2024-05-28 at 18:24:14 (-0800): Write file "Y:\work\att_networking_sims\emu_exp_prop\EBC-DD1\Power.fpp" successfully completed!
2024-05-28 at 18:24:15 (-0800): Writing file "Y:\work\att_networking_sims\emu_exp_prop\EBC-DD1\Path Loss.txt"....
2024-05-28 at 18:24:15 (-0800): Write file "Y:\work\att_networking_sims\emu_exp_prop\EBC-DD1\Path Loss.txt" successfully completed!
2024-05-28 at 18:24:15 (-0800): Writing file "Y:\work\att_networking_sims\emu_exp_prop\EBC-DD1\Path Loss.fpl"....
2024-05-28 at 18:24:15 (-0800): Write file "Y:\work\att_networking_sims\emu_exp_prop\EBC-DD1\Path Loss.fpl" successfully completed!
2024-05-28 at 18:24:15 (-0800):         Computation of LOS status started ...
2024-05-28 at 18:24:15 (-0800):         Computation of LOS status completed!
2024-05-28 at 18:24:15 (-0800): Writing file "Y:\work\att_networking_sims\emu_exp_prop\EBC-DD1\LOS.txt"....
2024-05-28 at 18:24:15 (-0800): Write file "Y:\work\att_networking_sims\emu_exp_prop\EBC-DD1\LOS.txt" successfully completed!
2024-05-28 at 18:24:15 (-0800): Writing file "Y:\work\att_networking_sims\emu_exp_prop\EBC-DD1\LOS.los"....
2024-05-28 at 18:24:16 (-0800): Write file "Y:\work\att_networking_sims\emu_exp_prop\EBC-DD1\LOS.los" successfully completed!
2024-05-28 at 18:24:16 (-0800):         Writing of str-file started ...
2024-05-28 at 18:24:18 (-0800):         Writing of str-file completed!
2024-05-28 at 18:24:18 (-0800):         Writing of ray-file started ...
2024-05-28 at 18:24:19 (-0800):         Writing of ray-file completed!
2024-05-28 at 18:24:19 (-0800): Free parameters of urban project started...
2024-05-28 at 18:24:19 (-0800):     - free geometry of prediction area
2024-05-28 at 18:24:19 (-0800):     - free transmitter information
2024-05-28 at 18:24:19 (-0800):     - free CNP parameters
2024-05-28 at 18:24:19 (-0800): Free parameters of urban project completed
2024-05-28 at 18:24:19 (-0800):  **************************************************************************************************************
2024-05-28 at 18:24:19 (-0800):  => Prediction of transmitter (antenna)
2024-05-28 at 18:24:19 (-0800):     "EBC-DD1" completed!!
2024-05-28 at 18:24:19 (-0800):  **************************************************************************************************************
2024-05-28 at 18:24:19 (-0800):  **************************************************************************************************************
2024-05-28 at 18:24:19 (-0800): Propagation predictions completed

Stop Day and Time: 2024-05-28 at 18:24:19 (-0800)
